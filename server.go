package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var client *mongo.Client

func getQuoteHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/get_quote" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	if r.Method != "GET" {
		http.Error(w, "Method is not supported.", http.StatusNotFound)
		return
	}

	quote := fetchRandomQuote(client)
	fmt.Fprintf(w, "%s\n", quote)
}

func main() {
	dbConnString := os.Getenv("DB_CONN_STRING")
	if dbConnString == "" {
		log.Fatal("Missing DB_CONN_STRING")
	}
	var err error
	client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI(dbConnString))
	if err != nil {
		panic(err)
	}
	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		panic(err)
	}

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}
	http.HandleFunc("/get_quote", getQuoteHandler)

	fmt.Printf("Starting server at port %s\n", httpPort)
	if err := http.ListenAndServe(":"+httpPort, nil); err != nil {
		log.Fatal(err)
	}
}
